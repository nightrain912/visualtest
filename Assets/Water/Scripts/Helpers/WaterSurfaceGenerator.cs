﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VisualTest.Helpers
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter))]
    public class WaterSurfaceGenerator : MonoBehaviour
    {
        [SerializeField] private Vector2Int m_verticesCount = new Vector2Int(10, 10);
        [SerializeField] private Rect m_verticesRect = new Rect(0, 0, 1, 1);
        [SerializeField] private Rect m_uvsRect = new Rect(0, 0, 1, 1);

        private List<Vector3> m_vertices = new List<Vector3>();
        private List<Color> m_colors = new List<Color>();
        private List<Vector2> m_uvs = new List<Vector2>();
        private List<int> m_triangles = new List<int>();

        private Mesh m_mesh;
        private MeshFilter m_cachedMeshFilter;

        private MeshFilter cachedMeshFilter
        {
            get
            {
                if (m_cachedMeshFilter == null)
                    m_cachedMeshFilter = GetComponent<MeshFilter>();

                return m_cachedMeshFilter;
            }
        }

        private void OnEnable()
        {
            PrepareData();
            PrepareMesh();
            BakeMesh();
        }

        private void OnValidate()
        {
            if (!isActiveAndEnabled || m_mesh == null)
                return;

            PrepareData();
            BakeMesh();
        }

        private void OnDisable()
        {
            ClearData();
            RemoveMesh();
        }

        private void PrepareMesh()
        {
            RemoveMesh();
            m_mesh = new Mesh();
            m_mesh.hideFlags = HideFlags.DontSave;
            m_mesh.MarkDynamic();
            cachedMeshFilter.sharedMesh = m_mesh;
        }

        private void RemoveMesh()
        {
            if (m_mesh == null)
                return;

            if (Application.isPlaying)
                Destroy(m_mesh);
            else
                DestroyImmediate(m_mesh);

            m_mesh = null;
            cachedMeshFilter.sharedMesh = null;
        }

        private void ClearData()
        {
            m_vertices.Clear();
            m_colors.Clear();
            m_uvs.Clear();
            m_triangles.Clear();
        }

        private void PrepareData()
        {
            ClearData();
            var vertexOffset = m_verticesRect.min;
            var uvOffset = m_uvsRect.min;
            var inverseDeltaSize = Vector2.one / (m_verticesCount - Vector2Int.one);
            var vertexDelta = m_verticesRect.size * inverseDeltaSize;
            var uvDelta = m_uvsRect.size * inverseDeltaSize;
            var index = 0;

            for (int x = 0; x < m_verticesCount.x; ++x)
            {
                for (int y = 0; y < m_verticesCount.y; ++y)
                {
                    var position = new Vector3(
                        vertexOffset.x + vertexDelta.x * x,
                        0,
                        vertexOffset.x + vertexDelta.y * y
                    );
                    m_vertices.Add(position);
                    var color = Color.clear;

                    if (y == 0)
                    {
                        color[0] = Random.Range(0.7f, 1.0f); // foam intensity
                        color[1] = Random.Range(0.0f, 1.0f); // foam time offset
                        color[2] = 0.5f;
                        color[3] = 1;
                    }
                    else if (y == 1)
                    {
                        color = m_colors[index - 1];
                        color[0] = 0;
                    }

                    m_colors.Add(color);
                    m_uvs.Add(new Vector2(
                                  uvOffset.x + uvDelta.x * x,
                                  uvOffset.x + uvDelta.y * y
                              ));
                    ++index;
                }
            }

            for (int x = 1; x < m_verticesCount.x; ++x)
            {
                for (int y = 1; y < m_verticesCount.y; ++y)
                {
                    var a = x * m_verticesCount.y + y;
                    var b = a - 1;
                    var c = a - 1 - m_verticesCount.y;
                    var d = a - m_verticesCount.y;
                    m_triangles.Add(a);
                    m_triangles.Add(b);
                    m_triangles.Add(c);
                    m_triangles.Add(a);
                    m_triangles.Add(c);
                    m_triangles.Add(d);
                }
            }
        }

        private void BakeMesh()
        {
            m_mesh.Clear(keepVertexLayout: false);
            m_mesh.SetVertices(m_vertices);
            m_mesh.SetUVs(0, m_uvs);
            m_mesh.SetColors(m_colors);
            m_mesh.SetTriangles(m_triangles, 0, calculateBounds: true);
            m_mesh.RecalculateNormals();
            m_mesh.RecalculateBounds();
        }
    }
}