﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VisualTest.Helpers
{
    [ExecuteInEditMode]
    public class Elevator : MonoBehaviour
    {
        void Update()
        {
            var pos = transform.position;
            pos.y = Mathf.Sin(Time.time) * 2;
            transform.position = pos;
        }
    }
}
