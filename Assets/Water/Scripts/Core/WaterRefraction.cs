﻿using UnityEngine;

namespace VisualTest.WaterTest
{
    public sealed class WaterRefraction : WaterEffect
    {
        [Space]
        [SerializeField, Range(-0.5f, 0.5f)] private float m_clipPlaneOffset = -0.05f;
        [SerializeField, Range(0, 100)] private float m_refractionFadeDepth = 5.0f;

        private static int m_refractionTextureId = Shader.PropertyToID("_RefractionTexture");
        private static int m_refractionFadeDepthId = Shader.PropertyToID("_RefractionFadeDepth");
        private static int m_refractionClipPlaneOffsetId = Shader.PropertyToID("_RefractionClipPlaneOffset");

        protected override string effectName => "Refraction";
        protected override string effectMaterialKeyword => "HAS_REFRACTION";

        private RenderTexture m_refractionTexture;

        protected override void RemoveElements()
        {
            base.RemoveElements();
            RemoveRefractionTexture();
        }

        private void RemoveRefractionTexture()
        {
            if (m_refractionTexture == null)
                return;

            CorrectDestroy(m_refractionTexture);
            m_refractionTexture = null;
        }

        protected override void PrepareCamera(Camera sourceCamera, Camera targetCamera)
        {
            sourceCamera.depthTextureMode |= DepthTextureMode.Depth;
            base.PrepareCamera(sourceCamera, targetCamera);
            targetCamera.clearFlags = CameraClearFlags.SolidColor;
            targetCamera.backgroundColor = Color.clear;
            Vector3 position = transform.position;
            Vector3 normal = transform.up;
            Vector4 clipPlane = Utils.CameraSpacePlane(targetCamera, position, -normal, 1.0f, m_clipPlaneOffset);
            Matrix4x4 projection = sourceCamera.CalculateObliqueMatrix(clipPlane);
            targetCamera.projectionMatrix = projection;
        }

        private void OnRenderObject()
        {
            Camera.current.depthTextureMode &= ~DepthTextureMode.Depth;
        }

        protected override void PrepareTextures(Vector2Int texturesSize)
        {
            if (m_refractionTexture == null || m_refractionTexture.width != texturesSize.x || m_refractionTexture.height != texturesSize.y)
            {
                RemoveRefractionTexture();
                m_refractionTexture = CreateRenderTexture(depthBits: 0, name: "Result");
            }
        }

        protected override void OnPreprocessing(Vector2Int texturesSize, Material targetMaterial)
        {
            RenderToTexture(m_refractionTexture, invertCulling: false);

            if (hasBlur)
            {
                var refractionWithBlur = Blur(m_refractionTexture);
                m_refractionTexture.DiscardContents();
                Graphics.Blit(refractionWithBlur, m_refractionTexture);
                RenderTexture.ReleaseTemporary(refractionWithBlur);
            }

            targetMaterial.SetTexture(m_refractionTextureId, m_refractionTexture);
            targetMaterial.SetFloat(m_refractionFadeDepthId, m_refractionFadeDepth);
            targetMaterial.SetFloat(m_refractionClipPlaneOffsetId, m_clipPlaneOffset);
        }
    }
}