﻿using UnityEngine;

namespace VisualTest.WaterTest
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Water))]
    public abstract class WaterEffect : MonoBehaviour
    {
        [Header("Texture adaptive size")]
        [SerializeField] private Vector2Int m_minTextureSize = new Vector2Int(8, 8);
        [SerializeField] private Vector2Int m_maxTextureSize = new Vector2Int(256, 256);
        [SerializeField, Range(1, 32)] private int m_textureScale = 8;


        [Header("Blur")]
        [SerializeField] private Shader m_blurShader = null;
        [SerializeField, Range(0, 10)] private int m_blurIterations = 2;
        [SerializeField, Range(0.0f, 2.0f)] private float m_blurSpread = 0.6f;

        protected Camera m_camera;
        protected Transform m_cameraTransform;
        private bool m_alreadyInRendering = false;
        private Water m_cachedWater;
        private static int? m_notWaterLayersMaskCached;
        private Vector2Int m_texturesCurrentSize;
        private Material m_blurMaterial;
        private static int m_blurSpreadId = Shader.PropertyToID("_BlurSpread");

        public Vector2Int minTextureSize
        {
            get
            {
                return m_minTextureSize;
            }
            set
            {
                m_minTextureSize = Utils.GetSizeAsPowerOf2(value);
            }
        }

        public Vector2Int maxTextureSize
        {
            get
            {
                return m_maxTextureSize;
            }
            set
            {
                m_maxTextureSize = Utils.GetSizeAsPowerOf2(value);
            }
        }

        public int textureScale
        {
            get
            {
                return m_textureScale;
            }
            set
            {
                m_textureScale = Mathf.Max(value, 1);
            }
        }

        private Water cachedWater
        {
            get
            {
                if (m_cachedWater == null)
                    m_cachedWater = GetComponent<Water>();

                return m_cachedWater;
            }
        }

        private static int notWaterLayersMaskCached
        {
            get
            {
                if (!m_notWaterLayersMaskCached.HasValue)
                    m_notWaterLayersMaskCached = ~(1 << LayerMask.NameToLayer("Water"));

                return m_notWaterLayersMaskCached.Value;
            }
        }

        protected abstract string effectName { get; }
        protected abstract string effectMaterialKeyword { get; }
        protected virtual void PrepareCamera(Camera sourceCamera, Camera targetCamera)
        {
            m_camera.CopyFrom(sourceCamera);
            m_camera.cullingMask &= notWaterLayersMaskCached;
            m_camera.depthTextureMode &= ~DepthTextureMode.Depth;
        }

        protected virtual void PrepareTextures(Vector2Int texturesSize) { }
        protected abstract void OnPreprocessing(Vector2Int texturesSize, Material targetMaterial);

        private void OnValidate()
        {
            m_minTextureSize = Utils.GetSizeAsPowerOf2(m_minTextureSize);
            m_maxTextureSize = Utils.GetSizeAsPowerOf2(m_maxTextureSize);
            m_textureScale = Mathf.Max(m_textureScale, 1);
        }

        protected Vector2Int GetSuitableSize(Vector2Int screenSize)
        {
            var size = new Vector2Int(screenSize.x / m_textureScale, screenSize.y / m_textureScale);
            size = Vector2Int.Max(Vector2Int.Min(size, m_maxTextureSize), m_minTextureSize);
            return Utils.GetSizeAsPowerOf2(size);
        }

        protected RenderTexture CreateTemporaryRenderTexture(Vector2Int size, int depthBits, string name, bool isPowerOfTwo)
        {
            var texture = RenderTexture.GetTemporary(
                              width: size.x,
                              height: size.y,
                              depthBuffer: depthBits
                          );
            texture.name = name + effectName + "Texture";
            texture.isPowerOfTwo = isPowerOfTwo;
            return texture;
        }

        protected RenderTexture CreateTemporaryRenderTexture(int depthBits, string name)
        {
            return CreateTemporaryRenderTexture(m_texturesCurrentSize, depthBits, name, isPowerOfTwo: true);
        }

        protected RenderTexture CreateRenderTexture(Vector2Int size, int depthBits, string name, bool isPowerOfTwo)
        {
            var texture = new RenderTexture(
                width: size.x,
                height: size.y,
                depth: depthBits
            );
            texture.name = name + effectName + "Texture";
            texture.isPowerOfTwo = isPowerOfTwo;
            return texture;
        }

        protected RenderTexture CreateRenderTexture(int depthBits, string name)
        {
            return CreateRenderTexture(m_texturesCurrentSize, depthBits, name, isPowerOfTwo: true);
        }

        protected virtual void RecreateElements()
        {
            RecreateCamera();
            RecreateBlur();
        }

        protected virtual void RemoveElements()
        {
            RemoveCamera();
            RemoveBlur();
        }

        private void OnEnable()
        {
            RecreateElements();
            cachedWater.waterMaterial.EnableKeyword(effectMaterialKeyword);
        }

        private void OnDisable()
        {
            RemoveElements();
            cachedWater.waterMaterial.DisableKeyword(effectMaterialKeyword);
        }

        private void OnWillRenderObject()
        {
            if (m_alreadyInRendering)
                return;

            m_alreadyInRendering = true;

            try
            {
                var sourceCamera = Camera.current;
                var sourceCameraSize = new Vector2Int(sourceCamera.pixelWidth, sourceCamera.pixelHeight);
                m_texturesCurrentSize = GetSuitableSize(sourceCameraSize);
                PrepareCamera(sourceCamera, m_camera);
                PrepareTextures(m_texturesCurrentSize);
                OnPreprocessing(m_texturesCurrentSize, cachedWater.waterMaterial);
            }
            finally
            {
                m_alreadyInRendering = false;
            }
        }

        private void RecreateBlur()
        {
            RemoveBlur();
            m_blurMaterial = new Material(m_blurShader);
            m_blurMaterial.hideFlags = HideFlags.HideAndDontSave;
        }

        private void RemoveBlur()
        {
            if (m_blurMaterial == null)
                return;

            CorrectDestroy(m_blurMaterial);
            m_blurMaterial = null;
        }

        protected void RenderToTexture(RenderTexture texture, bool invertCulling)
        {
            texture.DiscardContents();
            var previousCulling = GL.invertCulling;
            GL.invertCulling = invertCulling;
            m_camera.enabled = true;
            m_camera.targetTexture = texture;
            m_camera.Render();
            m_camera.targetTexture = null;
            m_camera.enabled = false;
            GL.invertCulling = previousCulling;
        }

        protected virtual void RecreateCamera()
        {
            RemoveCamera();
            var cameraGameObject = new GameObject(effectName + "Camera");
            cameraGameObject.hideFlags = HideFlags.HideAndDontSave;
            m_cameraTransform = cameraGameObject.transform;
            m_cameraTransform.SetParent(transform);
            m_camera = cameraGameObject.AddComponent<Camera>();
            m_camera.enabled = false;
        }

        protected virtual void RemoveCamera()
        {
            if (m_camera == null)
                return;

            CorrectDestroy(m_camera.gameObject);
            m_cameraTransform = null;
            m_camera = null;
        }

        protected void CorrectDestroy(Object obj)
        {
            if (Application.isPlaying)
                Destroy(obj);
            else
                DestroyImmediate(obj);
        }

        protected bool hasBlur => m_blurIterations > 0 || m_blurSpread == 0;

        protected RenderTexture Blur(RenderTexture source)
        {
            int width = source.width;
            int height = source.height;
            RenderTexture currentBuffer = RenderTexture.GetTemporary(width, height, 0);
            RenderTexture nextBuffer = RenderTexture.GetTemporary(width, height, 0);
            var material = m_blurMaterial;
            material.SetFloat(m_blurSpreadId, m_blurSpread);
            var sourceBuffer = source;

            for (int i = 0; i < m_blurIterations; ++i)
            {
                nextBuffer.DiscardContents();
                Graphics.Blit(sourceBuffer, nextBuffer, material, pass: 0);
                currentBuffer.DiscardContents();
                Graphics.Blit(nextBuffer, currentBuffer, material, pass: 1);
                sourceBuffer = currentBuffer;
            }

            RenderTexture.ReleaseTemporary(nextBuffer);
            return sourceBuffer;
        }
    }
}