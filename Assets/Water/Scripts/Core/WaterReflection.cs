﻿using UnityEngine;

namespace VisualTest.WaterTest
{
    public sealed class WaterReflection : WaterEffect
    {
        [Space]
        [SerializeField, Range(-0.5f, 0.5f)] private float m_clipPlaneOffset = 0.07f;

        private static int m_reflectionTextureId = Shader.PropertyToID("_ReflectionTexture");
        protected override string effectName => "Reflection";
        protected override string effectMaterialKeyword => "HAS_REFLECTION";

        private RenderTexture m_reflectionTexture;

        protected override void RemoveElements()
        {
            base.RemoveElements();
            RemoveReflectionTexture();
        }

        private void RemoveReflectionTexture()
        {
            if (m_reflectionTexture == null)
                return;

            CorrectDestroy(m_reflectionTexture);
            m_reflectionTexture = null;
        }

        protected override void PrepareCamera(Camera sourceCamera, Camera targetCamera)
        {
            base.PrepareCamera(sourceCamera, targetCamera);
            Vector3 position = transform.position;
            Vector3 normal = transform.up;
            float normalRatio = -Vector3.Dot(normal, position) - m_clipPlaneOffset;
            Vector4 reflectionPlane = new Vector4(normal.x, normal.y, normal.z, normalRatio);
            Matrix4x4 reflection = Matrix4x4.zero;
            Utils.CalculateReflectionMatrix(ref reflection, reflectionPlane);
            Vector3 oldPosition = sourceCamera.transform.position;
            Vector3 newPosition = reflection.MultiplyPoint(oldPosition);
            targetCamera.worldToCameraMatrix = sourceCamera.worldToCameraMatrix * reflection;
            Vector4 clipPlane = Utils.CameraSpacePlane(targetCamera, position, normal, 1.0f, m_clipPlaneOffset);
            Matrix4x4 projection = sourceCamera.CalculateObliqueMatrix(clipPlane);
            targetCamera.projectionMatrix = projection;
        }

        protected override void PrepareTextures(Vector2Int texturesSize)
        {
            if (m_reflectionTexture == null || m_reflectionTexture.width != texturesSize.x || m_reflectionTexture.height != texturesSize.y)
            {
                RemoveReflectionTexture();
                m_reflectionTexture = CreateRenderTexture(depthBits: 0, name: "Result");
            }
        }

        protected override void OnPreprocessing(Vector2Int textureSize, Material targetMaterial)
        {
            RenderToTexture(m_reflectionTexture, invertCulling: true);

            if (hasBlur)
            {
                var reflectionWithBlur = Blur(m_reflectionTexture);
                m_reflectionTexture.DiscardContents();
                Graphics.Blit(reflectionWithBlur, m_reflectionTexture);
                RenderTexture.ReleaseTemporary(reflectionWithBlur);
            }

            targetMaterial.SetTexture(m_reflectionTextureId, m_reflectionTexture);
        }
    }
}