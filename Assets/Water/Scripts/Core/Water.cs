﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VisualTest.WaterTest
{
    [ExecuteInEditMode]
    public class Water : MonoBehaviour
    {
        [System.Serializable]
        private struct WaveInfo
        {
            public float scale;
            public float weight;

            public float speed;
            public float speedOscillationForce;
            public float speedOscillationOffset;
            public float speedOscillationFrequency;

            public void Validate()
            {
                scale = Mathf.Max(scale, 0);
                weight = Mathf.Max(weight, 0);
            }
        }

        [SerializeField] Material m_waterMaterial = default;
        [SerializeField] float m_timeScale = 1.0f;

        [Space]
        [SerializeField] bool m_hasFoam = true;

        [Space]
        [SerializeField] Transform m_light = default;
        [SerializeField] bool m_hasLight = true;

        [Space]
        [SerializeField]
        private WaveInfo m_firstWave = new WaveInfo
        {
            scale = 0.3f,
            weight = 0.3f,
            speed = 0.3f,
            speedOscillationForce = 0.3f,
            speedOscillationOffset = 0.0f,
            speedOscillationFrequency = 1.1f
        };

        [SerializeField]
        private WaveInfo m_secondWave = new WaveInfo
        {
            scale = 0.7f,
            weight = 0.7f,
            speed = 1.0f,
            speedOscillationForce = 0.3f,
            speedOscillationOffset = 0.5f,
            speedOscillationFrequency = 1.3f
        };

        const string m_foamKeyword = "HAS_FOAM";
        const string m_lightKeyword = "HAS_LIGHT";
        readonly int m_wavesUvOffsetId = Shader.PropertyToID("_WavesUvOffset");
        readonly int m_waveSeparateScaleAndWeightId = Shader.PropertyToID("_WaveSeparateScaleAndWeight");
        readonly int m_timeId = Shader.PropertyToID("_WaterTime");
        readonly int m_lightPositionId = Shader.PropertyToID("_LightPosition");

        public Material waterMaterial
        {
            get
            {
                return m_waterMaterial;
            }
        }

        public bool hasFoam
        {
            get
            {
                return m_hasFoam;
            }
            set
            {
                if (m_hasFoam == value)
                    return;

                UpdateKeywords();
            }
        }

        public bool hasLight
        {
            get
            {
                return m_hasLight;
            }
            set
            {
                if (m_hasLight == value)
                    return;

                UpdateKeywords();
            }
        }

        private void UpdateKeywords()
        {
            if (m_waterMaterial == null)
                return;

            if (m_hasFoam)
                m_waterMaterial.EnableKeyword(m_foamKeyword);
            else
                m_waterMaterial.DisableKeyword(m_foamKeyword);

            if (m_hasLight)
                m_waterMaterial.EnableKeyword(m_lightKeyword);
            else
                m_waterMaterial.DisableKeyword(m_lightKeyword);
        }

        private float GetWaveSpeed(in WaveInfo wave, float time)
        {
            float oscillation = Mathf.Sin(time * wave.speedOscillationFrequency + Mathf.PI * 0.5f * wave.speedOscillationOffset) * wave.speedOscillationForce;
            return (oscillation + time) * wave.speed;
        }

        private void UpdateParameters()
        {
            if (m_waterMaterial == null)
                return;

            var time = Time.time * m_timeScale;

            if (!Application.isPlaying)
                time = 0;

            m_waterMaterial.SetFloat(m_timeId, time);
            var firstWave = new Vector2(1, 1);
            var secondWave = new Vector2(-1, 1);
            firstWave *= GetWaveSpeed(m_firstWave, time);
            secondWave *= GetWaveSpeed(m_secondWave, time);
            var wavesUvOffset = new Vector4(
                firstWave.x, firstWave.y, secondWave.x, secondWave.y
            );
            var summaryWeight = m_firstWave.weight + m_secondWave.weight;

            if (summaryWeight <= 0)
                summaryWeight = 1.0f;

            var waveSeparateScaleAndWeight = new Vector4(
                m_firstWave.scale,
                m_firstWave.weight / summaryWeight,
                m_secondWave.scale,
                m_secondWave.weight / summaryWeight
            );
            m_waterMaterial.SetVector(m_wavesUvOffsetId, wavesUvOffset);
            m_waterMaterial.SetVector(m_waveSeparateScaleAndWeightId, waveSeparateScaleAndWeight);

            if (m_light != null && m_hasLight)
                m_waterMaterial.SetVector(m_lightPositionId, m_light.position);
        }

        private void OnValidate()
        {
            UpdateKeywords();
            UpdateParameters();
        }

        private void OnEnable()
        {
            UpdateKeywords();
            UpdateParameters();
        }

        private void Update()
        {
            UpdateParameters();
        }
    }
}