﻿Shader "VisualTest/Water/Water"
{
    Properties
    {
        [NoScaleOffset] _ReflectionTexture ("Reflection", 2D) = "white" {}
        [NoScaleOffset] _RefractionTexture ("Refraction", 2D) = "white" {}
        [NoScaleOffset] _NormalTexture ("NormalTest", 2D) = "white" {}
        [NoScaleOffset] _FoamTexture ("Foam", 2D) = "white" {}

        [Space]
        _WaveSpeed ("Wave speed", Float) = 1
        _WaveStrength ("Wave strength", Range(0.001, 1)) = 1
        _WaveScale ("Wave scale", Range(0.01, 10)) = 1
        
        [Space]
        _SimpleReflectionColor ("Simple Reflection Color (without texturing)", Color) = (1, 1, 1, 1)
        _ReflectionColor ("Reflection Color (with texturing)", Color) = (1, 1, 1, 1)
        _ReflectionDistortion ("Reflection Distortion", Range(0, 1)) = 1

        [Space]
        _SimpleRefractionColor ("Refraction Color (without texturing)", Color) = (1, 1, 1, 1)
        _RefractionColor ("Refraction Color (with texturing)", Color) = (1, 1, 1, 1)
        _RefractionDistortion ("Refraction Distortion", Range(0, 1)) = 1

        [Space]
        _FoamSize ("Foam Size", Range(0.01, 3)) = 0.1
        _FoamIntensity ("Foam Intensity", Range(0, 1)) = 1
        _FoamRandomOffsetPercent ("Foam Random Offset Percent", Range(0, 1)) = 0.4
        _FoamAntialiasingPercent ("Foam Antialiasing Percent", Range(0, 1)) = 0.04
        _FoamSpeed ("Foam Speed", Float) = 1
        _FoamUvSpeed ("Foam UV Speed", Float) = 1
        _FoamDistanceSizeBetweenWaves ("Foam Distance Between Waves", Range(0, 5)) = 0

        [Space]
        _LightSpecularColor ("Light specular Color", Color) = (1, 1, 1, 1)
        _LightSpecularShininess ("_Light specular shininess", Float) = 10
        _LightPosition ("Light position", Vector) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" }
        LOD 200
        ZTest On
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass{

        CGPROGRAM
        
        #include "UnityCG.cginc"
        
        #pragma vertex vert
        #pragma fragment frag
        #pragma target 2.5
        #pragma multi_compile_local __ HAS_REFLECTION
        #pragma multi_compile_local __ HAS_REFRACTION
        #pragma multi_compile_local __ HAS_FOAM
        #pragma multi_compile_local __ HAS_LIGHT

#ifdef HAS_REFLECTION
        sampler2D _ReflectionTexture;
        half4 _ReflectionColor;
        half _ReflectionDistortion;

#else
        half4 _SimpleReflectionColor;
#endif

#ifdef HAS_REFRACTION
        sampler2D _RefractionTexture;
        sampler2D_float _LastCameraDepthTexture;
        half _RefractionFadeDepth;
        half _RefractionClipPlaneOffset;
        half4 _RefractionColor;
        half _RefractionDistortion;
#else
        half4 _SimpleRefractionColor;
#endif

#ifdef HAS_FOAM
        sampler2D _FoamTexture;
        half _FoamSize;
        half _FoamIntensity;
        half _FoamRandomOffsetPercent;
        half _FoamAntialiasingPercent;
        half _FoamSpeed;
        half _FoamUvSpeed;
        half _FoamDistanceSizeBetweenWaves;
#endif

        half _WaterTime;
        half _WaveSpeed;
        half4 _WavesUvOffset;
        half4 _WaveSeparateScaleAndWeight;
        half _WaveStrength;
        half _WaveScale;
        sampler2D _NormalTexture;

#ifdef HAS_LIGHT
        half3 _LightPosition;
        half4 _LightSpecularColor;
        half _LightSpecularShininess;
#endif

        struct v2f 
        {
            half4 pos : SV_POSITION;
            half2 uv : TEXCOORD0;
            half4 screenPos : TEXCOORD1;
            half3 normal : TEXCOORD2;
            half3 viewDir : TEXCOORD3;

#ifdef HAS_LIGHT
            half3 lightDirection : TEXCOORD4;
#endif

#ifdef HAS_FOAM
            half4 foamData : COLOR;
#endif
        };

        v2f vert(appdata_full v)
        {
            v2f o;
            o.pos = UnityObjectToClipPos(v.vertex);
            o.screenPos = ComputeScreenPos(o.pos);
            o.uv = v.texcoord.xy;
            o.normal = UnityObjectToWorldNormal(v.normal);
            o.viewDir = ObjSpaceViewDir(v.vertex);

#ifdef HAS_LIGHT
            half3 worldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
            o.lightDirection = normalize(_LightPosition - worldPosition);
#endif

#ifdef HAS_FOAM
            o.foamData = v.color;
#endif

            return o; 
        }

        inline half3 getReflection(half2 screenUV, half3 normal)
        {
#ifdef HAS_REFLECTION
            half2 uv = UNITY_PROJ_COORD(screenUV + normal.xz * _ReflectionDistortion);

            half3 reflection = _ReflectionColor.rgb * _ReflectionColor.a;
            reflection *= tex2D(_ReflectionTexture, uv).rgb;

            return reflection;
#else
            return _SimpleReflectionColor.rgb * _SimpleReflectionColor.a;
#endif
        }

        inline half4 getRefraction(half2 screenUV, half4 screenPos, half3 normal)
        {
#ifdef HAS_REFRACTION
            half2 uv = UNITY_PROJ_COORD(screenUV + normal.xz * _RefractionDistortion);

            half depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, uv));
            half depthRefractionCoefficient = (1.0 / _RefractionFadeDepth * (depth - screenPos.w));

            // remove negative values (above the water)
            depthRefractionCoefficient = lerp(1, saturate(depthRefractionCoefficient), step(_RefractionClipPlaneOffset, depthRefractionCoefficient));

            half4 textureRefraction = tex2D(_RefractionTexture, uv);
            half4 colorRefraction = half4(_RefractionColor.rgb * _RefractionColor.a, 1);
            half4 refraction = lerp(colorRefraction, textureRefraction, textureRefraction.a);

            return lerp(refraction, colorRefraction, depthRefractionCoefficient);

#else
            return _SimpleRefractionColor;
#endif
        }

        inline half4 applyFoam(half4 color, half4 foamData, half2 uv) {
#ifdef HAS_FOAM
            half foamIntensity = foamData.x;
            half foamTimeOffset = foamData.y;
            half2 foamUvDirection = foamData.zw * 2 - 1;

            half animatedFoamIntensity = -_WaterTime * _FoamSpeed + foamIntensity / _FoamSize + foamTimeOffset * _FoamRandomOffsetPercent;
            animatedFoamIntensity = frac(animatedFoamIntensity / (_FoamDistanceSizeBetweenWaves + 1)) * (_FoamDistanceSizeBetweenWaves + 1);

            // add small gradient to the top of the edge
            animatedFoamIntensity *= 1 - saturate((animatedFoamIntensity - 1 + _FoamAntialiasingPercent)/ _FoamAntialiasingPercent);
            
            foamIntensity = foamIntensity * animatedFoamIntensity * _FoamIntensity;
            
            half4 foamColor = tex2D(_FoamTexture, uv + foamUvDirection * _WaterTime * _FoamUvSpeed);

            color = lerp(color, foamColor, foamIntensity);
#endif

            return color;
        }

        inline half3 mergeNormals(half3 textureNormal, half3 vertexNormal) {
            return normalize(vertexNormal + half3(textureNormal.x, 0, textureNormal.y) * _WaveStrength);
        }

        inline half3 getNormal(half2 uv, half3 vertexNormal) {
            half3 pixelNormal0 = UnpackNormal(tex2D(_NormalTexture, (uv.xy + _WavesUvOffset.xy) * _WaveScale * _WaveSeparateScaleAndWeight.x)) * _WaveSeparateScaleAndWeight.y;
            half3 pixelNormal1 = UnpackNormal(tex2D(_NormalTexture, (uv.xy + _WavesUvOffset.zw) * _WaveScale * _WaveSeparateScaleAndWeight.z)) * _WaveSeparateScaleAndWeight.w;

            return mergeNormals(
                pixelNormal0 + pixelNormal1,
                vertexNormal
            );
        }

        half4 frag( v2f i ) : SV_Target
        {
            half2 screenUV = i.screenPos.xy / i.screenPos.w;
            half3 normal = getNormal(i.uv, i.normal);
            half3 viewDir = normalize(i.viewDir);

            half simpleFresnelRatio = dot(viewDir, normal);

            half4 reflection = half4(getReflection(screenUV, normal), 1);
            half4 refraction = getRefraction(screenUV, i.screenPos, normal);
            half4 color = lerp(reflection, refraction, simpleFresnelRatio);

#ifdef HAS_FOAM
            color = applyFoam(color, i.foamData, i.uv);
#endif

#ifdef HAS_LIGHT
            half3 lightDirectionReflected = reflect(-i.lightDirection, normal);
            half specularValue = pow(max(dot(viewDir, lightDirectionReflected), 0), _LightSpecularShininess);

            color.rgb += _LightSpecularColor.rgb * _LightSpecularColor.a * specularValue;
#endif

            color.rgb *= color.a;

            return color;
        }

        ENDCG
        }
    }
    FallBack "Diffuse"
}
