﻿using UnityEngine;
using System.Collections.Generic;

namespace VisualTest
{
    public class ColoredTerrainGenerator : MonoBehaviour
    {
        [System.Serializable]
        public struct Lod
        {
            public MeshFilter meshFilter;
            public Vector2Int verticesCount;
            public bool forBaking;

            public Lod GetValidated()
            {
                var result = this;
                result.verticesCount = Vector2Int.Max(verticesCount, new Vector2Int(2, 2));
                return result;
            }

            public Lod ChangeFilter(MeshFilter newMeshFilter)
            {
                var result = this;
                result.meshFilter = newMeshFilter;
                return result;
            }
        }

        [Header("Mesh settings")]
        [SerializeField]
        private Lod[] m_lods = new Lod[1]
        {
            new Lod{
                meshFilter = null,
                verticesCount = new Vector2Int(25, 25),
                forBaking = false
            }
        };

        [SerializeField] private Rect m_verticesRect = new Rect(-0.5f, -0.5f, 1, 1);
        [SerializeField] private Rect m_uvsRect = new Rect(0, 0, 1, 1);


        [Space]
        [Header("Terrain settings")]
        [SerializeField] private int m_seed = 0;
        [Tooltip("Each component of the vector is a base height for the layer")]
        [SerializeField] private Vector4 m_layersHeight = Vector4.zero;

        [Tooltip("Each component of the vector is a additional random height ratio for the layer")]
        [SerializeField] private Vector4 m_layersRandomHeightRatio = Vector4.zero;

        [Tooltip("Each component of the vector is a additional random height ratio for the layer")]
        [SerializeField, Range(0, 10)] private int m_smoothSteps = 5;

        private const int m_layersCount = 4;

        private List<Vector3> m_vertices = new List<Vector3>();
        private List<Color> m_colors = new List<Color>();
        private List<Vector2> m_uvs = new List<Vector2>();
        private List<int> m_triangles = new List<int>();

        private Mesh[] m_cachedMeshes;

        public Lod[] lods
        {
            get
            {
                return m_lods;
            }
            set
            {
                m_lods = value;
                ValidateLods();
            }
        }

        public Rect verticesRect
        {
            get
            {
                return m_verticesRect;
            }
            set
            {
                m_verticesRect = value;
            }
        }

        public Rect uvsRect
        {
            get
            {
                return m_uvsRect;
            }
            set
            {
                m_uvsRect = value;
            }
        }

        public Vector4 layersHeight
        {
            get
            {
                return m_layersHeight;
            }
            set
            {
                m_layersHeight = value;
            }
        }

        public Vector4 layersRandomHeightRatio
        {
            get
            {
                return m_layersRandomHeightRatio;
            }
            set
            {
                m_layersRandomHeightRatio = value;
            }
        }

        public int smoothSteps
        {
            get
            {
                return m_smoothSteps;
            }
            set
            {
                m_smoothSteps = Mathf.Clamp(value, 0, 10);
            }
        }

        private void OnValidate()
        {
            ValidateLods();
            m_smoothSteps = Mathf.Clamp(m_smoothSteps, 0, 10);
        }

        private void ValidateLods()
        {
            for (int i = 0, count = m_lods.Length; i < count; ++i)
                lods[i] = lods[i].GetValidated();

            GenerateMeshesCache();
        }

        public void Regenerate()
        {
            const float heightPositionScale = 5.0f;
            var previousRandomState = Random.state;
            Random.InitState(m_seed);
            var perlinMatrices = CreatePerlinMatricesForLayers();
            var additionalHeightMatrix = CreatePerlinMatrix(heightPositionScale);
            Random.state = previousRandomState;
            GenerateMeshesCache();

            for (int i = 0, count = m_lods.Length; i < count; ++i)
            {
                var lod = m_lods[i];
                var uvs = lod.forBaking ? new Rect(0, 0, 1, 1) : m_uvsRect;
                GenerateData(lod.verticesCount, perlinMatrices, additionalHeightMatrix, uvs);
                BakeMesh(lod.meshFilter, i);
            }
        }

        public void GenerateRandom()
        {
            m_seed = Random.Range(int.MinValue, int.MaxValue);
            Regenerate();
        }

        private void GenerateMeshesCache()
        {
            var count = m_lods.Length;
            var existingCount = 0;

            if (m_cachedMeshes == null)
            {
                m_cachedMeshes = new Mesh[count];
            }
            else
            {
                existingCount = m_cachedMeshes.Length;
                System.Array.Resize(ref m_cachedMeshes, count);
            }

            for (int i = existingCount; i < count; ++i)
            {
                var mesh = new Mesh();
                m_cachedMeshes[i] = mesh;
                mesh.name = "Terrain" + i.ToString();
            }
        }

        private Matrix4x4 CreatePerlinMatrix(float scale)
        {
            return Matrix4x4.TRS(
                       new Vector2(Random.value * 1000, Random.value * 1000),
                       Quaternion.Euler(0, 0, Random.value * 360),
                       Vector3.one * scale
                   );
        }

        private Matrix4x4[] CreatePerlinMatricesForLayers()
        {
            var count = 3; // lerp(lerp(x,y), lerp(z,w))
            var matrices = new Matrix4x4[count];
            var perlinScale = 1.0f;

            for (int i = 0; i < count; ++i)
                matrices[i] = CreatePerlinMatrix(perlinScale);

            return matrices;
        }

        private static float SmoothStep(float t)
        {
            return 3 * t * t - 2 * t * t * t;
        }

        private static float SmoothStepN(float t, int count)
        {
            for (int i = 0; i < count; ++i)
                t = SmoothStep(t);

            return t;
        }

        private float SmoothPerlin(Vector2 position, Matrix4x4 transformationMatrix)
        {
            var transformedPosition = transformationMatrix.MultiplyPoint3x4(position);
            var value = Mathf.PerlinNoise(transformedPosition.x, transformedPosition.y);
            return SmoothStepN(value, m_smoothSteps);
        }

        private Vector4 GetLayers(Vector2 position, Matrix4x4[] perlinMatrices)
        {
            var firstValue = SmoothPerlin(position, perlinMatrices[0]);
            var secondValue = SmoothPerlin(position, perlinMatrices[1]);
            var finalValue = SmoothPerlin(position, perlinMatrices[2]);
            Vector4 layers = Vector4.zero;
            layers[0] = (firstValue) * (finalValue);
            layers[1] = (1 - firstValue) * (finalValue);
            layers[2] = (secondValue) * (1 - finalValue);
            layers[3] = (1 - secondValue) * (1 - finalValue);
            return layers;
        }

        private void ClearData()
        {
            m_vertices.Clear();
            m_colors.Clear();
            m_uvs.Clear();
            m_triangles.Clear();
        }

        private void PrepareContainers(int verticesCount, int trianglesCount)
        {
            m_vertices.Capacity = verticesCount;
            m_colors.Capacity = verticesCount;
            m_uvs.Capacity = verticesCount;
            m_triangles.Capacity = trianglesCount;
        }

        private void GenerateData(Vector2Int verticesCount, Matrix4x4[] perlinMatrices, Matrix4x4 additionalHeightMatrix, Rect uvsRect)
        {
            ClearData();
            PrepareContainers(
                verticesCount: verticesCount.x * verticesCount.y,
                trianglesCount: (verticesCount.x - 1) * (verticesCount.y - 1) * 6
            );
            var vertexOffset = m_verticesRect.min;
            var uvOffset = uvsRect.min;
            var inverseDeltaSize = Vector2.one / (verticesCount - Vector2Int.one);
            var vertexDelta = m_verticesRect.size * inverseDeltaSize;
            var uvDelta = uvsRect.size * inverseDeltaSize;

            for (int x = 0; x < verticesCount.x; ++x)
            {
                for (int y = 0; y < verticesCount.y; ++y)
                {
                    var position = new Vector3(
                        vertexOffset.x + vertexDelta.x * x,
                        vertexOffset.x + vertexDelta.y * y,
                        0
                    );
                    Vector4 layers = GetLayers(position, perlinMatrices);
                    // По неясным причинам у unity нет оператора умножения 4х-мерных векторов
                    // var height = Vector4.Dot(layers * m_layersHeight, Vector4.one);
                    var height =
                        layers.x * m_layersHeight.x +
                        layers.y * m_layersHeight.y +
                        layers.z * m_layersHeight.z +
                        layers.w * m_layersHeight.w;
                    var randomHeightRatio =
                        layers.x * m_layersRandomHeightRatio.x +
                        layers.y * m_layersRandomHeightRatio.y +
                        layers.z * m_layersRandomHeightRatio.z +
                        layers.w * m_layersRandomHeightRatio.w;
                    height += SmoothPerlin(position, additionalHeightMatrix) * randomHeightRatio;
                    position.z = -height;
                    m_vertices.Add(position);
                    m_uvs.Add(new Vector2(
                                  uvOffset.x + uvDelta.x * x,
                                  uvOffset.x + uvDelta.y * y
                              ));
                    m_colors.Add(new Color(layers.x, layers.y, layers.z, layers.w));
                }
            }

            for (int x = 1; x < verticesCount.x; ++x)
            {
                for (int y = 1; y < verticesCount.y; ++y)
                {
                    var a = x * verticesCount.y + y;
                    var b = a - 1;
                    var c = a - 1 - verticesCount.y;
                    var d = a - verticesCount.y;
                    m_triangles.Add(a);
                    m_triangles.Add(b);
                    m_triangles.Add(c);
                    m_triangles.Add(a);
                    m_triangles.Add(c);
                    m_triangles.Add(d);
                }
            }
        }

        private void BakeMesh(MeshFilter meshFilter, int lodIndex)
        {
            if (meshFilter == null)
            {
                Debug.LogErrorFormat("Not found mesh filter for lod {0} in '{1}'", lodIndex, gameObject.name);
                return;
            }

            var uvsChannel = 0;
            var subMeshIndex = 0;
            Mesh mesh = m_cachedMeshes[lodIndex];
            meshFilter.sharedMesh = mesh;
            mesh.Clear(keepVertexLayout: false);
            mesh.SetVertices(m_vertices);
            mesh.SetColors(m_colors);
            mesh.SetUVs(uvsChannel, m_uvs);
            mesh.SetTriangles(m_triangles, subMeshIndex, calculateBounds: true);
            mesh.RecalculateNormals();
        }
    }
}