﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VisualTest
{
    public class TextureSplattingController : MonoBehaviour
    {
        [Header("Materials")]
        [SerializeField] Material[] m_affectedMaterials = new Material[0];
        [SerializeField, Range(1, 4)] int m_texturesCount = 4;

        private void OnValidate()
        {
            const int maxTexturesCount = 4;
            var keywords = new string[maxTexturesCount];

            for (int i = 0; i < maxTexturesCount; ++i)
                keywords[i] = "TEXTURES_COUNT_" + (i + 1).ToString();

            foreach (var material in m_affectedMaterials)
            {
                for (int i = 0; i < maxTexturesCount; ++i)
                {
                    var isSuitableKeyword = (i + 1) == m_texturesCount;

                    if (isSuitableKeyword)
                        material.EnableKeyword(keywords[i]);
                    else
                        material.DisableKeyword(keywords[i]);
                }
            }
        }
    }
}