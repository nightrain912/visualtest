﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

namespace VisualTest
{
    public class TextureSplattingBakingWindow : EditorWindow
    {
        struct Source
        {
            public MeshFilter meshFilter;
            public MeshRenderer renderer;
        }

        private TextureSplattingController m_controller;
        private int m_sourceIndex = -1;
        private Material m_targetMaterial;
        private string m_targetTexturePath;
        private Vector2Int m_targetTextureSize;

        public void Init(TextureSplattingController controller)
        {
            m_controller = controller;
        }

        private bool FindController()
        {
            if (m_controller != null)
                return true;

            var selectedTransform = Selection.activeTransform;

            if (selectedTransform == null)
                return false;

            m_controller = selectedTransform.GetComponent<TextureSplattingController>();
            return m_controller != null;
        }

        private void OnGUI()
        {
            if (!FindController())
            {
                EditorGUILayout.HelpBox("Not found selected TextureSplattingController", MessageType.Error);
                return;
            }

            GUILayout.Label("Source data settings", EditorStyles.boldLabel);
            var source = DrawSourceMeshAndMaterial();
            GUILayout.Label("Target texture settings", EditorStyles.boldLabel);
            DrawTargetSettings();
            GUILayout.Label("Generation", EditorStyles.boldLabel);
            DrawGeneration(source);
        }

        private Source? DrawSourceMeshAndMaterial()
        {
            var suitableObjects = GetSuitableSources();
            var suitableNames = suitableObjects.Select(pair => pair.meshFilter.gameObject.name).ToArray();
            m_sourceIndex = EditorGUILayout.Popup("Source", m_sourceIndex, suitableNames);
            Source? source = null;

            if (m_sourceIndex >= 0)
                source = suitableObjects[m_sourceIndex];

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Mesh", source?.meshFilter?.sharedMesh, typeof(Mesh), allowSceneObjects : true);
            EditorGUILayout.ObjectField("Material", source?.renderer?.sharedMaterial, typeof(Material), allowSceneObjects : true);
            EditorGUI.EndDisabledGroup();
            return source;
        }

        private void DrawTargetSettings()
        {
            m_targetMaterial = EditorGUILayout.ObjectField("Target Material", m_targetMaterial, typeof(Material), allowSceneObjects: true) as Material;
            var targetTexture = m_targetMaterial == null ? null : m_targetMaterial.mainTexture;

            if (string.IsNullOrEmpty(m_targetTexturePath))
                m_targetTexturePath = AssetDatabase.GetAssetPath(targetTexture);

            EditorGUILayout.BeginHorizontal();
            m_targetTexturePath = EditorGUILayout.TextField("Target texture path", m_targetTexturePath);

            if (GUILayout.Button("...", EditorStyles.miniButton, GUILayout.MaxWidth(30)))
            {
                if (string.IsNullOrEmpty(m_targetTexturePath))
                    m_targetTexturePath = "Assets/Texture.png";

                var newPath = EditorUtility.SaveFilePanel(
                                  title: "Save *.png texture",
                                  directory: Path.GetDirectoryName(m_targetTexturePath),
                                  defaultName: Path.GetFileName(m_targetTexturePath),
                                  extension: "png");

                if (!string.IsNullOrEmpty(newPath))
                    m_targetTexturePath = MakeRelativePath(Application.dataPath, newPath);
            }

            EditorGUILayout.EndHorizontal();
            var previousTexture = AssetDatabase.LoadAssetAtPath<Texture>(m_targetTexturePath);

            if (previousTexture != null && m_targetTextureSize.x <= 1 && m_targetTextureSize.y <= 1)
            {
                m_targetTextureSize.x = previousTexture.width;
                m_targetTextureSize.y = previousTexture.height;
            }

            m_targetTextureSize = EditorGUILayout.Vector2IntField("Texture size", m_targetTextureSize);
            m_targetTextureSize = Vector2Int.Max(m_targetTextureSize, Vector2Int.one);
        }

        void DrawGeneration(Source? source)
        {
            bool status = true;

            if (!source.HasValue)
            {
                EditorGUILayout.HelpBox("Not found source mesh and material", MessageType.Warning);
                status = false;
            }
            else if (source.Value.renderer.sharedMaterial == null)
            {
                EditorGUILayout.HelpBox("Not found source material", MessageType.Warning);
                status = false;
            }
            else if (source.Value.meshFilter.sharedMesh == null)
            {
                EditorGUILayout.HelpBox("Not found source mesh", MessageType.Warning);
                status = false;
            }
            else if (string.IsNullOrEmpty(m_targetTexturePath))
            {
                EditorGUILayout.HelpBox("Invalid target path", MessageType.Warning);
                status = false;
            }

            EditorGUI.BeginDisabledGroup(!status);

            if (!GUILayout.Button("Generate"))
                status = false;

            EditorGUI.EndDisabledGroup();

            if (!status)
                return;

            var descriptor = new RenderTextureDescriptor(width: m_targetTextureSize.x, height: m_targetTextureSize.y);
            descriptor.colorFormat = RenderTextureFormat.Default;
            var targetTexture = RenderTexture.GetTemporary(descriptor);
            targetTexture.DiscardContents();
            var material = source.Value.renderer?.sharedMaterial;
            var defaultPass = 4;
            var defaultPassName = "DEFERRED";
            int pass = material?.FindPass(defaultPassName) ?? defaultPass;
            CustomBlit(targetTexture, source.Value.meshFilter?.sharedMesh, material, pass);
            var textureToSave = new Texture2D(descriptor.width, descriptor.height);
            var previousRenderTexture = RenderTexture.active;
            RenderTexture.active = targetTexture;
            textureToSave.ReadPixels(new Rect(0, 0, descriptor.width, descriptor.height), 0, 0);
            textureToSave.Apply();
            RenderTexture.active = previousRenderTexture;
            RenderTexture.ReleaseTemporary(targetTexture);
            var data = ImageConversion.EncodeToPNG(textureToSave);
            File.WriteAllBytes(m_targetTexturePath, data);
            AssetDatabase.ImportAsset(m_targetTexturePath, ImportAssetOptions.ForceUpdate);
            AssetDatabase.Refresh();
            var resultTexture = AssetDatabase.LoadAssetAtPath<Texture2D>(m_targetTexturePath);

            if (resultTexture != null)
                m_targetMaterial.mainTexture = resultTexture;
        }

        private void CustomBlit(RenderTexture target, Mesh mesh, Material material, int pass)
        {
            var previousRenderTexture = RenderTexture.active;
            RenderTexture.active = target;
            GL.PushMatrix();
            material.SetPass(pass);
            GL.LoadOrtho();
            var bounds = mesh.bounds;
            var bounds2d = new Rect(bounds.min, bounds.size);
            var matrix = Matrix4x4.TRS(Vector2.zero - bounds2d.min / bounds2d.size, Quaternion.identity, Vector3.one / bounds2d.size);
            Graphics.DrawMeshNow(mesh, matrix);
            GL.PopMatrix();
            RenderTexture.active = previousRenderTexture;
        }

        private List<Source> GetSuitableSources()
        {
            var suitableObjects = new List<Source>();
            var meshFilters = m_controller.GetComponentsInChildren<MeshFilter>();

            foreach (var filter in meshFilters)
            {
                var renderer = filter.GetComponent<MeshRenderer>();

                if (renderer == null)
                    continue;

                suitableObjects.Add(new Source { meshFilter = filter, renderer = renderer });
            }

            return suitableObjects;
        }

        private static string MakeRelativePath(string fromPath, string toPath)
        {
            if (string.IsNullOrEmpty(fromPath))
                throw new ArgumentNullException("fromPath");

            if (string.IsNullOrEmpty(toPath))
                throw new ArgumentNullException("toPath");

            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme)
                return toPath;

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            string relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);

            return relativePath;
        }
    }
}