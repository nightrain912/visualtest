﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace VisualTest
{
    [CustomEditor(typeof(TextureSplattingController), editorForChildClasses: true)]
    public class TextureSplattingControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DrawBakeButton();
        }

        private void DrawBakeButton()
        {
            if (!GUILayout.Button("Bake material to texture"))
                return;

            var window = EditorWindow.GetWindow<TextureSplattingBakingWindow>();
            window.Init(target as TextureSplattingController);
            window.Show();
        }
    }
}