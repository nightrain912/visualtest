﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace VisualTest
{
    [CustomEditor(typeof(ColoredTerrainGenerator), editorForChildClasses: true)]
    [CanEditMultipleObjects]
    public class ColoredTerrainGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.Space();
            DrawFindLodsButton();
            EditorGUILayout.Space();
            DrawGenerateButtons();
        }

        private void DrawFindLodsButton()
        {
            if (!GUILayout.Button("Find lod meshes"))
                return;

            var meshFilters = new List<MeshFilter>();

            foreach (ColoredTerrainGenerator generator in targets)
            {
                meshFilters.Clear();
                meshFilters.AddRange(generator.GetComponentsInChildren<MeshFilter>());
                var lods = generator.lods;
                var count = meshFilters.Count;
                System.Array.Resize(ref lods, count);

                for (var i = 0; i < count; ++i)
                    lods[i] = lods[i].ChangeFilter(meshFilters[i]);

                generator.lods = lods;
            }
        }

        private void DrawGenerateButtons()
        {
            if (GUILayout.Button("Regenerate meshes"))
            {
                foreach (ColoredTerrainGenerator generator in targets)
                    generator.Regenerate();
            }

            if (GUILayout.Button("Generate random meshes"))
            {
                foreach (ColoredTerrainGenerator generator in targets)
                    generator.GenerateRandom();
            }
        }
    }
}