﻿fixed3 simpleBlend(
        sampler2D texture0, sampler2D texture1,
        fixed2 uv0, fixed2 uv1,
        fixed coefficient
    )
{
    return lerp(tex2D(texture1, uv1).rgb, tex2D(texture0, uv0).rgb, saturate(coefficient));
}

fixed3 simpleBlend(
        sampler2D texture0, sampler2D texture1, sampler2D texture2,
        fixed2 uv0, fixed2 uv1, fixed2 uv2,
        fixed2 coefficients
    )
{
    coefficients = saturate(coefficients);
 
    fixed sum = coefficients.x + coefficients.y;
    fixed lastCoefficient = max(0, 1 - sum);
    
    sum += lastCoefficient;

    return (
        tex2D(texture0, uv0).rgb * coefficients.x +
        tex2D(texture1, uv1).rgb * coefficients.y +
        tex2D(texture2, uv2).rgb * lastCoefficient
    ) / sum;
}

fixed3 simpleBlend(
        sampler2D texture0, sampler2D texture1, sampler2D texture2, sampler2D texture3,
        fixed2 uv0, fixed2 uv1, fixed2 uv2, fixed2 uv3,
        fixed3 coefficients
    )
{
    coefficients = saturate(coefficients);
 
    fixed sum = coefficients.x + coefficients.y + coefficients.z;
    fixed lastCoefficient = max(0, 1 - sum);
    
    sum += lastCoefficient;

    return (
        tex2D(texture0, uv0).rgb * coefficients.x +
        tex2D(texture1, uv1).rgb * coefficients.y +
        tex2D(texture2, uv2).rgb * coefficients.z +
        tex2D(texture3, uv3).rgb * lastCoefficient
    ) / sum;
}

fixed3 heightBlend(
        sampler2D texture0, sampler2D texture1,
        fixed2 uv0, fixed2 uv1,
        fixed coefficient,
        fixed blendingDepth
    )
{
    fixed4 data0 = tex2D(texture0, uv0);
    fixed4 data1 = tex2D(texture1, uv1);

    coefficient = saturate(coefficient);
    fixed2 fullCoefficients = fixed2(coefficient, 1 - coefficient);
    fixed2 heights = fixed2(data0.a, data1.a);

    fixed2 weights = heights + fullCoefficients;

    fixed maxWeight = max(weights.x, weights.y);
    fixed2 topLayerWeights = max(weights - maxWeight + blendingDepth, 0);

    return (
        data0.rgb * topLayerWeights.x +
        data1.rgb * topLayerWeights.y
    ) / (topLayerWeights.x + topLayerWeights.y);
}

fixed3 heightBlend(
        sampler2D texture0, sampler2D texture1, sampler2D texture2,
        fixed2 uv0, fixed2 uv1, fixed2 uv2,
        fixed2 coefficients,
        fixed blendingDepth
    )
{
    fixed4 data0 = tex2D(texture0, uv0);
    fixed4 data1 = tex2D(texture1, uv1);
    fixed4 data2 = tex2D(texture2, uv2);

    coefficients = saturate(coefficients);
    fixed3 fullCoefficients = fixed3(coefficients, max(0, 1 - coefficients.x - coefficients.y));
    fixed3 heights = fixed3(data0.a, data1.a, data2.a);

    fixed3 weights = heights + fullCoefficients;

    fixed maxWeight = max(max(weights.x, weights.y), weights.z);
    fixed3 topLayerWeights = max(weights - maxWeight + blendingDepth, 0);

    return (
        data0.rgb * topLayerWeights.x +
        data1.rgb * topLayerWeights.y +
        data2.rgb * topLayerWeights.z
    ) / (topLayerWeights.x + topLayerWeights.y + topLayerWeights.z);
}

fixed3 heightBlend(
        sampler2D texture0, sampler2D texture1, sampler2D texture2, sampler2D texture3,
        fixed2 uv0, fixed2 uv1, fixed2 uv2, fixed2 uv3,
        fixed3 coefficients,
        fixed blendingDepth
    )
{
    fixed4 data0 = tex2D(texture0, uv0);
    fixed4 data1 = tex2D(texture1, uv1);
    fixed4 data2 = tex2D(texture2, uv2);
    fixed4 data3 = tex2D(texture3, uv3);

    coefficients = saturate(coefficients);
    fixed4 fullCoefficients = fixed4(coefficients, max(0, 1 - coefficients.x - coefficients.y - coefficients.z));
    fixed4 heights = fixed4(data0.a, data1.a, data2.a, data3.a);

    fixed4 weights = heights + fullCoefficients;

    fixed maxWeight = max(max(weights.x, weights.y), max(weights.z, weights.w));
    fixed4 topLayerWeights = max(weights - maxWeight + blendingDepth, 0);

    return (
        data0.rgb * topLayerWeights.x +
        data1.rgb * topLayerWeights.y +
        data2.rgb * topLayerWeights.z +
        data3.rgb * topLayerWeights.w
    ) / (topLayerWeights.x + topLayerWeights.y + topLayerWeights.z + topLayerWeights.w);
}
