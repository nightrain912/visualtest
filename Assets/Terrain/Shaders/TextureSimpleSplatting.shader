﻿Shader "VisualTest/TextureSimpleSplatting"
{
    Properties
    {
        _Texture0 ("Texture 0", 2D) = "white" {}
        _Texture1 ("Texture 1", 2D) = "white" {}
        _Texture2 ("Texture 2", 2D) = "white" {}
        _Texture3 ("Texture 3", 2D) = "white" {}
    }
    SubShader
    {
        Tags {
            "PreviewType" = "Plane"
        }

        LOD 100

        CGPROGRAM

        #pragma surface mainSurface Lambert
        #pragma target 2.5
        #pragma multi_compile_local __ TEXTURES_COUNT_1 TEXTURES_COUNT_2 TEXTURES_COUNT_3

        #include "Splattings.cginc"

        struct Input
        {
            fixed2 uv_Texture0;
            fixed2 uv_Texture1;
            fixed2 uv_Texture2;
            fixed2 uv_Texture3;
            fixed4 color : COLOR;
        };

        sampler2D _Texture0;
        sampler2D _Texture1;
        sampler2D _Texture2;
        sampler2D _Texture3;

        void mainSurface(Input input, inout SurfaceOutput o)
        {
#if TEXTURES_COUNT_1
            o.Albedo = tex2D(_Texture0, input.uv_Texture0);

#elif TEXTURES_COUNT_2
            o.Albedo = simpleBlend(
                _Texture0, _Texture1,
                input.uv_Texture0, input.uv_Texture1,
                input.color
            );

#elif TEXTURES_COUNT_3
            o.Albedo = simpleBlend(
                _Texture0, _Texture1, _Texture2,
                input.uv_Texture0, input.uv_Texture1, input.uv_Texture2,
                input.color
            );

#else
            o.Albedo = simpleBlend(
                _Texture0, _Texture1, _Texture2, _Texture3,
                input.uv_Texture0, input.uv_Texture1, input.uv_Texture2, input.uv_Texture3,
                input.color
            );
#endif
        }
        ENDCG
    }
    FallBack "Diffuse"
}
